package com.emerio.dian.api;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.emerio.dian.jpa.entity.Nilai;
import com.emerio.dian.jpa.entity.Pelajaran;
import com.emerio.dian.jpa.entity.Siswa;
import com.emerio.dian.jpa.repo.NilaiRepo;
import com.emerio.dian.jpa.repo.PelajaranRepo;
import com.emerio.dian.jpa.repo.SiswaRepo;

@RestController
@RequestMapping(path = "/api")
public class Api {

	@Autowired
	private PelajaranRepo pljRepo;

	@Autowired
	private SiswaRepo sisRepo;

	@Autowired
	private NilaiRepo nliRepo;

	/**
	 * http://localhost:8080/api/nil/rpt?noinduk=001&pelajaran=matematika
	 * @param req
	 * @return
	 */
	@GetMapping(path = "/nil/rpt")
	public NilaiRep rpt(HttpServletRequest req) {
		NilaiRep res = new NilaiRep();
		try {
			res.setCode(0);
			res.setMessage("OK");
			String noidk = req.getParameter("noinduk");
			String nmplj = req.getParameter("pelajaran");
			
			Pelajaran plj = pljRepo.findByNama(nmplj);
			Siswa sis = sisRepo.findByNoInduk(noidk);
			List<Nilai> nils = new ArrayList<>();
			if (sis != null && plj != null) {
				nils.add(nliRepo.findBySiswaAndPelajaran(sis, plj));
			} else if (sis != null) {
				nils.addAll(nliRepo.findBySiswa(sis));
			} else if (plj != null) {
				nils.addAll(nliRepo.findByPelajaran(plj));
			} else {
				nliRepo.findAll().iterator().forEachRemaining(nils::add);
			}
			
			List<RepItem> records = new ArrayList<>();
			nils.forEach(nil -> {
				RepItem record = new RepItem();
				record.setNilai(nil.getNilai());
				record.setPelajaran(nil.getPelajaran().getNama());
				record.setNama(nil.getSiswa().getNama());
				record.setNoInduk(nil.getSiswa().getNoInduk());
				
				records.add(record);
			});
			
			res.setRecords(records);
			
		} catch (Exception e) {
			res.setMessage(e.getMessage());
			res.setCode(-99);
		}
		
		return res;
	}

	/**
	 * http://localhost:8080/api/nil/upd?noinduk=001&pelajaran=matematika&nilai=80
	 * @param req
	 * @return
	 */
	@GetMapping(path = "/nil/upd")
	public Result updNil(HttpServletRequest req) {
		Result res = new Result();
		try {
			res.setCode(0);
			res.setMessage("OK");
			String noidk = req.getParameter("noinduk");
			String nmplj = req.getParameter("pelajaran");
			Double nil = Double.valueOf(req.getParameter("nilai"));
			
			Pelajaran plj = pljRepo.findByNama(nmplj);
			Siswa sis = sisRepo.findByNoInduk(noidk);
			Nilai nli = nliRepo.findBySiswaAndPelajaran(sis, plj);
			nli.setNilai(nil);
			
			nliRepo.save(nli);
		} catch (Exception e) {
			res.setMessage(e.getMessage());
			res.setCode(-99);
		}
		
		return res;
	}

	/**
	 * http://localhost:8080/api/plj/del?nama=agama
	 * @param req
	 * @return
	 */
	@GetMapping(path = "/plj/del")
	public Result delPlj(HttpServletRequest req) {
		Result res = new Result();
		try {
			res.setCode(0);
			res.setMessage("OK");
			String nama = req.getParameter("nama");
			Pelajaran plj = pljRepo.findByNama(nama);
			pljRepo.delete(plj);
		} catch (Exception e) {
			res.setMessage(e.getMessage());
			res.setCode(-99);
		}
		
		return res;
	}
	
}
