package com.emerio.dian.api;

import java.util.List;

public class NilaiRep extends Result {

	private List<RepItem> records;

	public List<RepItem> getRecords() {
		return records;
	}

	public void setRecords(List<RepItem> records) {
		this.records = records;
	}
	
	
}
