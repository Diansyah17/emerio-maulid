package com.emerio.dian.api;

public class RepItem {

	private String noInduk;
	
	private String nama;
	
	private String pelajaran;
	
	private Double nilai;

	public String getNoInduk() {
		return noInduk;
	}

	public void setNoInduk(String noInduk) {
		this.noInduk = noInduk;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getPelajaran() {
		return pelajaran;
	}

	public void setPelajaran(String pelajaran) {
		this.pelajaran = pelajaran;
	}

	public Double getNilai() {
		return nilai;
	}

	public void setNilai(Double nilai) {
		this.nilai = nilai;
	}
	
	
	
}
