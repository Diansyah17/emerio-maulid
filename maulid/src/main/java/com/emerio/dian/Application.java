package com.emerio.dian;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.Reader;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.emerio.dian.jpa.entity.Nilai;
import com.emerio.dian.jpa.entity.Pelajaran;
import com.emerio.dian.jpa.entity.Siswa;
import com.emerio.dian.jpa.repo.NilaiRepo;
import com.emerio.dian.jpa.repo.PelajaranRepo;
import com.emerio.dian.jpa.repo.SiswaRepo;

@SpringBootApplication
public class Application {

	@Autowired
	private SiswaRepo sisRepo;

	@Autowired
	private PelajaranRepo pljRepo;

	@Autowired
	private NilaiRepo nliRepo;

	@PostConstruct
	private void postConstruct() {
		
		try {
			Reader rdr = new FileReader("data/siswa.csv");
			BufferedReader brdr = new BufferedReader(rdr);
			
			String line;
			do {
				line = brdr.readLine();
				System.out.println(line);
				
				if (line == null) {
					continue;
				}
				
				String[] splits = line.split(",");
				
				Siswa ent = new Siswa();
				ent.setKelas(splits[2]);
				ent.setNama(splits[1]);
				ent.setNoInduk(splits[0]);
				
				sisRepo.save(ent);
				
			} while (line != null);
			
			brdr.close();
			rdr.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			Pelajaran ent;
			
			ent = new Pelajaran();
			ent.setNama("matematika");
			pljRepo.save(ent);
			
			ent = new Pelajaran();
			ent.setNama("agama");
			pljRepo.save(ent);
			
			ent = new Pelajaran();
			ent.setNama("sejarah");
			pljRepo.save(ent);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			Reader rdr = new FileReader("data/nilai.csv");
			BufferedReader brdr = new BufferedReader(rdr);
			
			String line;
			do {
				line = brdr.readLine();
				System.out.println(line);
				
				if (line == null) {
					continue;
				}
				
				String[] splits = line.split(",");
				
				Siswa sis = sisRepo.findByNoInduk(splits[0]);
				if (sis == null) {
					continue;
				}
				
				Pelajaran plj = pljRepo.findByNama(splits[1]);
				if (plj == null) {
					continue;
				}
				
				Double nil = Double.valueOf(splits[2]);
				Nilai nli = new Nilai();
				nli.setNilai(nil);
				nli.setPelajaran(plj);
				nli.setSiswa(sis);
				nliRepo.save(nli);
				
			} while (line != null);
			
			brdr.close();
			rdr.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public static void main(String[] args) throws Exception {
		//startH2();
		
		SpringApplication.run(Application.class, args);
	}
	
}
