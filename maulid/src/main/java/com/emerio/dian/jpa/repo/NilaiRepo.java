package com.emerio.dian.jpa.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.emerio.dian.jpa.entity.Nilai;
import com.emerio.dian.jpa.entity.Pelajaran;
import com.emerio.dian.jpa.entity.Siswa;

public interface NilaiRepo extends CrudRepository<Nilai, Long> {

	List<Nilai> findBySiswa(Siswa siswa);

	List<Nilai> findByPelajaran(Pelajaran pelajaran);

	Nilai findBySiswaAndPelajaran(Siswa siswa, Pelajaran pelajaran);
	
}
