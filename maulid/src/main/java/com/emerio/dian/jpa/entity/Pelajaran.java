package com.emerio.dian.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Pelajaran extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4915840126387454030L;

	@Column(name = "nama_plj", unique = true)
	private String nama;
	
	public String getNama() {
		return nama;
	}
	
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	
}
