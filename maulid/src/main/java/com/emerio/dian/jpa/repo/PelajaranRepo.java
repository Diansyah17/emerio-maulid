package com.emerio.dian.jpa.repo;

import org.springframework.data.repository.CrudRepository;

import com.emerio.dian.jpa.entity.Pelajaran;

public interface PelajaranRepo extends CrudRepository<Pelajaran, Long> {

	Pelajaran findByNama(String nama);

}
