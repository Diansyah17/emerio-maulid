package com.emerio.dian.jpa.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Base implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8359497817956719523L;

	@Id
	@GeneratedValue
	private Long id;

	public Long getId() {
		return id;
	}
	
}
