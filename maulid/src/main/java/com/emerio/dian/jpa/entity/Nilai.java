package com.emerio.dian.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Nilai extends Base {

	/**
	 * 
	 */
	private static final long serialVersionUID = -361092980531611683L;

	@OneToOne
	@JoinColumn(name = "siswa_id")
	private Siswa siswa;
	
	@OneToOne
	@JoinColumn(name = "pelajaran_id")
	private Pelajaran pelajaran;
	
	@Column(name = "nilai")
	private Double nilai;
	
	public Siswa getSiswa() {
		return siswa;
	}
	
	public void setSiswa(Siswa siswa) {
		this.siswa = siswa;
	}
	
	public Double getNilai() {
		return nilai;
	}
	
	public Pelajaran getPelajaran() {
		return pelajaran;
	}
	
	public void setPelajaran(Pelajaran pelajaran) {
		this.pelajaran = pelajaran;
	}
	
	public void setNilai(Double nilai) {
		this.nilai = nilai;
	}
	
}
