package com.emerio.dian.jpa.repo;

import org.springframework.data.repository.CrudRepository;

import com.emerio.dian.jpa.entity.Siswa;

public interface SiswaRepo extends CrudRepository<Siswa, Long> {

	Siswa findByNoInduk(String noInduk);
	
}
