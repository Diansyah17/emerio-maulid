package com.emerio.dian.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Siswa extends Base  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7003024565622940685L;

	@Column(name = "no_induk", unique = true)
	private String noInduk;
	
	@Column(name = "nama")
	private String nama;
	
	@Column(name = "kelas")
	private String kelas;
	
	public String getKelas() {
		return kelas;
	}
	
	public String getNama() {
		return nama;
	}
	
	public String getNoInduk() {
		return noInduk;
	}
	
	public void setKelas(String kelas) {
		this.kelas = kelas;
	}
	
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public void setNoInduk(String noInduk) {
		this.noInduk = noInduk;
	}
	
}
